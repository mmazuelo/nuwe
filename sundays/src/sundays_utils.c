#include "../include/sundays.h"
#include <stdlib.h>
#include <string.h>

#define DIA_INVALIDO ((cal_t){-1, -1, -1, -1})

int	dia_semana(int y, int m, int d)
{
	int	e_arr[12] = {0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4};
	int	f_arr[4] = {0, 5, 3, 1};
	int	c, g, e, f, w;

	if (m < 3)
		y = y - 1;

	c = y / 100;
	g = y % 100;
	e = e_arr[m - 1];
	f = f_arr[(c % 4)];
	w = (d + e + f + g + g/4) % 7;
	return (w == 0 ? 7 : w);
}

int	days_month(int year, int month)
{
	int	leap;
	int	dm[13] = {-1, 31, 28, 31, 30, 31, 30, 31,\
			31, 30, 31, 30, 31};

	leap = ((year % 4 == 0) ^ (year % 100 == 0) ^ (year % 400 == 0));

	if (leap)
		dm[2] = 29;
	return (dm[month]);
}

cal_t	set_day(int year, int month, int day)
{
	cal_t	fecha;
	int	max_days;

	max_days = days_month(year, month);

	if (month < 1 || month > 12)
		return (DIA_INVALIDO);
	else if (day < 1 || day > max_days)
		return (DIA_INVALIDO);

	fecha.year = year;
	fecha.month = month;
	fecha.day = day;
	fecha.weekday = dia_semana(year, month, day);

	return (fecha);
}

cal_t	day_add(cal_t fecha, int interval, const char *mode)
{
	int	max_days;
	int	year = fecha.year;
	int	month = fecha.month;
	int	day = fecha.day;

	if (strcmp(mode, "day") ==  0)
	{
		day += interval;
		while (day < 1)
		{
			month--;
			if (month < 1)
			{
				year--;
				month = 12;
			}
			day += days_month(year, month);
		}

		max_days = days_month(year, month);
		while (day > max_days)
		{
			day -= max_days;
			month++;
			if (month > 12)
			{
				year++;
				month = 1;
			}
			max_days = days_month(year, month);
		}
	}
	else if (strcmp(mode, "month") == 0)
	{
		month += interval;
		while (month < 1)
		{
			year--;
			month += 12;
		}
		while (month > 12)
		{
			year++;
			month -= 12;
		}

		max_days = days_month(year, month);
		if (day > max_days)
			day = max_days;
	}
	else if (strcmp(mode, "year") == 0)
	{
		year += interval;
		max_days = days_month(year, month);
		if (day > max_days)
			day = max_days;
	} 
	else
		return (DIA_INVALIDO);

	return (set_day(year, month, day));
}

static int	is_leap_year(cal_t date)
{
	return ((date.year % 4 == 0) ^ (date.year % 100 == 0) ^ (date.year % 400 == 0));
}

int	date_diff(cal_t start, const cal_t end)
{
	int	days = 0;

	while (start.year < end.year)
	{
		days += is_leap_year(start) ? 366 : 365;
		start = day_add(start, 1, "year");
	}

	while (start.month < end.month)
	{
		days += days_month(start.year, start.month);
		start = day_add(start, 1, "month");
	}

	days += end.day - start.day;

	return (days);
}
