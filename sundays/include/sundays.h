#ifndef	SUNDAYS_H
#define SUNDAYS_H

typedef struct	Calendar
{
	int	year;
	int	month;
	int	day;
	int	weekday;
}	cal_t;

int	dia_semana(int y, int m, int d);
int	days_month(int year, int month);
int	date_diff(cal_t start, const cal_t end);
cal_t	set_day(int year, int month, int day);
cal_t	day_add(cal_t fecha, int interval, const char *mode);

#endif
