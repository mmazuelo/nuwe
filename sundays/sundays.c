/* ******************************************************************************************************************** */
/*	You are given the following information, but you may prefer to do some research for yourself.			*/
/*															*/
/*	    1 Jan 1900 was a Monday.											*/
/*	    Thirty days has September, April, June and November. 							*/
/*	    All the rest have thirty-one, 										*/
/*	    Saving February alone, Which has twenty-eight, rain or shine. And on leap years, twenty-nine.		*/
/*	    A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.	*/
/*															*/
/*	How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?	*/
/*	Program Author: mmazuelo											*/
/* ******************************************************************************************************************** */

#include <stdio.h>
#include <sundays.h>

int	main(void)
{
	cal_t 	inicio;
	cal_t	fin;
	cal_t 	day;
	int	dom;
	int	delta;

	inicio = set_day(1901, 1, 1);
	fin = set_day(2000, 12, 31);

	dom = 0;
	delta = date_diff(inicio, fin); //36524;
	for (int i = 0; i < delta; i++)
	{
		day = day_add(inicio, i, "day");
		if (day.day == 1 && day.weekday == 7)
			dom += 1;
	}
	printf("Entre el día %.2i/%.2i/%.4i", inicio.day, inicio.month, inicio.year);
	printf(" y el %.2i/%.2i/%.4i hay %i", fin.day, fin.month, fin.year, dom);
	printf(" domingos que inician el mes.\n");
	return (0);
}
